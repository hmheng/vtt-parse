import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Scanner;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;


public class Main {
	
	static String[] items;
	
	static String[] vttLines;
	
	static String[] timePoints;
	
	static String content = "";
	
	static double durationInSeconds = 0.0F;
	
	public static void main(String[] args) {

		System.out.println("Started");
		
		getFiles();
		
		writeFiles();
		
		for (int i = 0; i < items.length; i++) {
			parseFile(items[i]);
		}
		
	}

	

	/* (non-Java-doc)
	 * @see java.lang.Object#Object()
	 */
	public Main() {
		super();
	}
	
	public static void getFiles() {
		String p = Paths.get("./sound-input").toString();
		
		String[] _items = new File(p).list();
	
		int count = (int) (_items.length / 2);
		
		int j = 0;
		
		items = new String[count];
		
		for (int i = 0; i < _items.length; i++) {
			if (_items[i].contains(".vtt")) {
				items[j] = _items[i];
				j++;
			}
		}
	
	}
	
	public static void writeFiles() {
		//System.out.println(items[0]);
	}

	public static void parseFile(String fn) {
		
		fn = fn.substring(0, fn.lastIndexOf('.'));
		
		System.out.println(fn);
		
		getVTT(fn);
		
		getAudioPoints(fn);
		
		editVTT();
		
		writeVTT(fn);
	}
	

	public static void getVTT(String fn) {
		
		vttLines = new String[1];
		
		String[] tempVtt = new String[1024];
		
		Scanner sc;
		try {
			sc = new Scanner(new File("sound-input/"+fn+".vtt"));
			int i = 0;
			while(sc.hasNextLine()){
				tempVtt[i] = sc.nextLine();
			    i++;
			}
			vttLines = new String[(tempVtt.length + 1)];
			for (i = 0; i < tempVtt.length; i++) {
				if (tempVtt[i] != null) {
					vttLines[i] = tempVtt[i];
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public static void writeVTT(String fn) {
				
		for (int i = 0; i < vttLines.length; i++) {
			if (vttLines[i] != null) {
				content += vttLines[i]+"\n";
				System.out.println(vttLines[i]);
			}
		}
		
		PrintWriter writer;
	
		try {
			writer = new PrintWriter("VTT-output/"+fn+".vtt", "UTF-8");
			writer.write(content);
			writer.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void getAudioPoints(String fn) {
		
		AudioInputStream audioInputStream;
		try {
			audioInputStream = AudioSystem.getAudioInputStream(new File("sound-input/"+fn+".wav").getAbsoluteFile());
			byte[] bytes = new byte[(int) (audioInputStream.getFrameLength()) * (audioInputStream.getFormat().getFrameSize())];
			audioInputStream.read(bytes);
			AudioFormat format = audioInputStream.getFormat();
			long frames = audioInputStream.getFrameLength();
			durationInSeconds = (frames+0.0) / format.getFrameRate();

			// Get amplitude values for each audio channel in an array.
			System.out.println(getUnscaledAmplitude(bytes, 1, durationInSeconds));

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedAudioFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	public static int[][] getUnscaledAmplitude(byte[] eightBitByteArray, int nbChannels, double durationInSeconds)
	{
		
		int[][] toReturn = new int[nbChannels][eightBitByteArray.length / (2 * nbChannels)];
	    int index = 0;

	    double inrange = 0, outofrange = 0;
	    
	    int[] beepPoints = new int[50];
	    
	    int beepCounter = 0;
	    
	    //System.out.println("xxx");
	    //System.out.println(eightBitByteArray.length);
	    
	    for (int audioByte = 0; audioByte < eightBitByteArray.length;)
	    {
	        for (int channel = 0; channel < nbChannels; channel++)
	        {
	        	
	        	
	            // Do the byte to sample conversion.
	            int low = (int) eightBitByteArray[audioByte];
	            audioByte++;
	            int high = (int) eightBitByteArray[audioByte];
	   	            audioByte++;
	            int sample = (high << 8) + (low & 0x00ff);
	            //System.out.println(sample);
	            int absSample = Math.abs(sample);
	            
	            if ((absSample > 25) && (absSample < 35)) {
	               	inrange++;
	            	outofrange = 0;
	            }

	            if ((absSample < 25) && (absSample > 35)) {
	               	inrange = 0;
	            	outofrange++;	            
	            }
	            if (inrange > 20700) {
	            	inrange = 0;
	             	outofrange = 0;
	             	beepPoints[beepCounter] = index;
	             	beepCounter++;
	             	//System.out.println("stop");
	             	//System.exit(0);
	            }
	            if (outofrange > 10000) {
	            	inrange = 0;
	            }
	            
	            toReturn[channel][index] = sample;
	        }
	        index++;
	    }
	    timePoints = new String[beepPoints.length];
	    for (int i = 0; i < beepPoints.length; i++) {
	    	if (beepPoints[i] > 0) {
	    		float calc =  (((float) beepPoints[i] / index) * (float) durationInSeconds);
	    		//System.out.println(i+"::::"+calc);
	    		timePoints[i] = String.valueOf(calc);
	    	}
	    }

	    return toReturn;
	}
	public static void editVTT() {
		int j = 0;
		String startTime = "00:00:00.000", endTime = "00:00:00.000";
		
		
		for (int i = 0; i < vttLines.length; i++) {
			if (vttLines[i] != null) {
				if (vttLines[i].contains("00:00:00.000 --> 00:00:00.000")) {
					endTime = getEndTime(timePoints[j]);
					vttLines[i] = startTime+" --> "+endTime;
					startTime = getStartTime(timePoints[j]);
					j++;
				}				
			}
		}
	}
	public static String getStartTime(String startTime) {
		if (startTime != null) { 
			float time = Float.parseFloat(startTime) + 0.2F;
			int hours = (int) (time / 3600);
			int minutes = (int) ((time - (hours * 3600)) / 60);
			int seconds = (int) (time - (hours * 3600) - (minutes * 60));
			int hundreths = (int) ((time - Math.floor(time)) * 1000);
			return String.format("%02d", hours)+":"+String.format("%02d", minutes)+":"+String.format("%02d", seconds)+"."+String.format("%03d", hundreths);
		} else {
			
			return "00:00:00.000";
		}
	}
	public static String getEndTime(String endTime) {
		
		
		if (endTime == null) { 
			endTime = String.valueOf(durationInSeconds);
		}
		
		float time = (float) (Float.parseFloat(endTime) - 1);
		int hours = (int) (time / 3600);
		int minutes = (int) ((time - (hours * 3600)) / 60);
		int seconds = (int) (time - (hours * 3600) - (minutes * 60));
		int hundreths = (int) ((time - Math.floor(time)) * 1000);
		return String.format("%02d", hours)+":"+String.format("%02d", minutes)+":"+String.format("%02d", seconds)+"."+String.format("%03d", hundreths);
		
	}
}